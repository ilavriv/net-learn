﻿using System;
using System.Collections.Generic;

namespace list_example
{
    class Program
    {
        static void Main(string[] args)
        {
            WorkingWithString();
        }

        public static void WorkingWithString() {
            var names = new List<string> { "<name>", "Anna", "Felipe" };

            foreach (var name in names)
            {
                Console.WriteLine($"Hello, {name.ToUpper()}");
            }

            Console.WriteLine();

            names.Add("Jack");
            names.Remove("Anna");
            names.Add("Genne");


            names.Sort();

            foreach (var name in names)
            {
                Console.WriteLine($"Hello, {name.ToUpper()}");
            }

            Console.WriteLine($"My name is {names[0]}");
            Console.WriteLine($"I've added {names[2]} and {names[3]} to the list");

            Console.WriteLine($"The list has {names.Count} people in it");

            var index = names.IndexOf("Felipe");
            Console.WriteLine($"The name {names[index]} is at index {index}");

            var notFound = names.IndexOf("Not Found");
            Console.WriteLine($"When an item is not found, IndexOf returns {notFound}");
        }
    }
}

﻿using System;

namespace classes
{
    class Program
    {
        static void Main(string[] args)
        {
            var account = new BankAccount("<name>", 500);
            account.MakeDeposit(200, DateTime.Now, "Pay for rent");
            account.MakeWithdraw(100, DateTime.Now, "Back to friend");
            

            Console.WriteLine(account.GetAccountHistory());
        }
    }
}

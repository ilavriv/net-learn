﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

namespace console_example
{
    class Program
    {
        static void Main(string[] args)
        {
            // ShowTeleprompter().Wait();

            ShowInput().Wait();
        }

        static IEnumerable<string> ReadFrom(string file)
        {
            string line;

            using(var reader = File.OpenText(file))
            {   
                while ((line = reader.ReadLine()) != null)
                {
                    var words = line.Split(' ');
                    foreach (var word in words)
                    {
                        yield return word + " ";
                    }
                    yield return Environment.NewLine;
                }
            }
        }

        private static async Task ShowTeleprompter()
        {
            var words = ReadFrom("poem.txt");
            foreach (var word in words)
            {
                Console.Write(word);
                if(!string.IsNullOrWhiteSpace(word))
                {
                    await Task.Delay(200);
                }
            }
        }

        private static async Task ShowInput()
        {
            var delay = 200;
            Action work = () => {
                do {
                    var key = Console.ReadKey(true);
                    if(key.KeyChar == '>')
                    {
                        delay -= 10;
                    }
                    else if(key.KeyChar == '<')
                    {
                        delay += 10;
                    }
                    else if (key.KeyChar == 'X' || key.KeyChar == 'x')
                    {
                        break;
                    }
                } while(true);
            };

            await Task.Run(work);
        }
    }
}
